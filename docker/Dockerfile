####
# This Dockerfile is used in order to build the application and run it using npm start:prod command
#
# Build the image with:
#
# npm run build:docker
#
# Then run the container using:
#
# npm run start:docker
#
###
FROM node:15-alpine3.10 as build
WORKDIR /app

# copy only the package files across, so we can cache the result of install
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install --concurrency 2

# copy all files (that arent being ignored in .dockerignore)
COPY . .

# build the application, creating the dist folder in the build docker image
RUN npm run build

# now take a fresh image and install only what we need for production
FROM node:15-alpine3.10 as run
WORKDIR /app

# copy over the dist and package files
COPY --from=build /app/dist dist
COPY --from=build /app/package-lock.json package-lock.json
COPY --from=build /app/package.json package.json

# install only the production packages (as we have built the dist already)
RUN npm install --only=prod

# env vars that can be optionally set for the application
ENV MONGO_DB_CONNECTION_URL=
ENV JOKE_API_CONNECTION_STRING=
ENV GENERATE_OPENAPI_SPEC=

# on run use npm to start the application in production mode
ENTRYPOINT npm run start:prod