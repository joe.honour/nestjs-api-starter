// user for application to connect with
db.createUser({
  user: 'application-user',
  pwd: 'password',
  roles: [
    {
      role: 'readWrite',
      db: 'example-api-db',
    },
  ],
});

// user for connecting with Robo 3T locally
db.createUser({
  user: 'robo-user',
  pwd: 'password',
  roles: [
    {
      role: 'dbOwner',
      db: 'example-api-db',
    },
  ],
});
