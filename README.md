# nestjs-api-starter

a starter project you can clone and adapt to speed up development of APIs to a production standard. This project contains:

- openapi generation from models/controllers
- the use of test containers to easily spin up and connect to a database (mongodb) during integration tests
- the use of wire mock (via test containers) to easily stub external dependencies during integration tests
- docker support for local (and production) development, with an optimised final image (less than 150mb in size)
- docker-compose support for local debugging of the app with a database connection
- ci/cd implemented with gitlab ci, basic build, test, and docker image creation steps.
- TODO: health checks
- TODO: json logging
- TODO: helm example for k8s
- TODO: dynamic log level changing at runtime (similar to JMX)
- TODO: promethesus integration
- TODO: production grade process monitoring (RAM/cpu/memory allocs)


## Installation

to get started once you have clones this repo, run the following.

```bash
$ npm install
```

install the following vscode extensions:
- ESLint
- Jest Runner

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Debugging the app

if you want to debug the app, use the launch configuration 'Debug API' in vscode, and you are good to go.

if you wish to debug tests, simply right click on the test and 'Debug Jest'.

## Running the app in docker

```bash
# build the docker image
$ npm run build:docker

# starts in docker (expect mongodb running on localhost)
$ npm run start:docker

# turns on just dependencies for the app (i.e. mongodb)
$ npm run compose:up

# turns on the dependencies and the app within docker
$ npm run compose:up:app

# turns off anything running in docker compose
$ npm run compose:down
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Open API

this app generates its [open api schema](./openapi.json) from the annotated models/controllers, and saves it to file. This means when you change these models you will get a diff in git of what that means for your consumers.

```bash
# generate openapi schema
$ npm run openapi:generate
```

when running the application, if you are in development mode (on by default), you can go to [http://localhost:3000/swagger-ui/](http://localhost:3000/swagger-ui/) to view the swagger ui. This is turned off in any other mode.

## Test Containers - Mongodb

during integration tests its always best to test against a real db, this can help verify: 
- the libraries you have brought in work against the version of the db you are expecting to integrate with
- you are configuring the libraries correctly
- the operations you are performing actually do what you expect!

in order to do this with ease, and per test file, we use test containers to turn the db on and off, see [MongoTestContainer](./test/mongo.test.container.ts)

## Test Containers - WireMock

during integration tests its always best to make as realistic http calls when integrating with other services. A mock or stub offers very little value compared with a full network request/response. A real request can help:
- verify headers are being sent correctly
- verify encoding/decoding is as expected
- verify any async operations are being correctly buffered, especially when dealing with response sizes greater than a few TCP packets.

in order to do this with ease, I have provided a test container that wraps [WireMock](http://wiremock.org/). This container then lets your tests configure responses based on requests it recieves, allowing you to validate your application talks to downstream systems correctly, see [WireMockTestContainer](./test/wiremock.test.container.ts)

## Support

This inital repository was built by Joe Honour(joe.honour@and.digital). I am by no means a JS expert, but i enjoy building and testing things with test containers, wiremock, docker, and kubernetes. Reach out to me if you have any questions or issues using this repo as a starting point for your API projects, or have further enhancements you think are useful. :) 
