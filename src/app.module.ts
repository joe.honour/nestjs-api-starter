import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JokeModule } from './joke/joke.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';

/**
 * provides any root resources we need for the application (i.e controllers / config / db connections).
 * the reason we use the mongo async initaliser is so that the config can be injected at runtime, then
 * during tests we have time to set the appropriate env variables so the config gets created correctly.
 */
@Module({
  imports: [
    ConfigModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        uri: config.mongoDbConnectionUrl,
      }),
      inject: [ConfigService],
    }),
    JokeModule,
  ],
})
export class AppModule {}
