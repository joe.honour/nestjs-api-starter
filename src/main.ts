import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { buildOpenApiDoc, generateOpenApiSpec } from './openapi.generator';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const openapi = buildOpenApiDoc(app);

  if (process.env.NODE_ENV === 'development') {
    SwaggerModule.setup('swagger-ui', app, openapi);
  }

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(3000);
}

/**
 * if you set the GENERATE_OPENAPI_SPEC variable, we generate the spec and do not start the application.
 * this is so we can easil plug in to the nest runtime, without needing to do anything fancy.
 */
if (process.env.GENERATE_OPENAPI_SPEC) {
  generateOpenApiSpec();
} else {
  bootstrap();
}
