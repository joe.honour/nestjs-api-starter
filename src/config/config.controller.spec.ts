import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from './config.service';

describe('ConfigService', () => {
  let configSevice: ConfigService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [ConfigService],
    }).compile();
    configSevice = app.get<ConfigService>(ConfigService);
  });

  it('should return default mongo connection string', () => {
    expect(configSevice.mongoDbConnectionUrl).toBe(
      'mongodb://application-user:password@localhost:27017/example-api-db',
    );
  });
});
