import { Injectable } from '@nestjs/common';

/**
 * provides access to application configuration.
 * configuration can be set via environment variables or, if no variable provided, a default will be used.
 */
@Injectable()
export class ConfigService {
  public readonly mongoDbConnectionUrl: string;
  public readonly externalJokeApiConnectionString: string;

  constructor() {
    this.mongoDbConnectionUrl =
      process.env.MONGO_DB_CONNECTION_URL ||
      'mongodb://application-user:password@localhost:27017/example-api-db';

    this.externalJokeApiConnectionString =
      process.env.JOKE_API_CONNECTION_STRING || 'http://api.jokes.one';
  }
}
