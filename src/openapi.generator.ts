import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { writeFileSync } from 'fs';
import { AppModule } from './app.module';

export function buildOpenApiDoc(app: INestApplication): OpenAPIObject {
  const options = new DocumentBuilder()
    .setTitle('NestJS API Starter - Joke API')
    .setDescription(
      'an example project that has everything you need to deliver high quality APIs to production',
    )
    .setVersion('1.0')
    .addTag('jokes')
    .build();

  return SwaggerModule.createDocument(app, options);
}

/**
 * generates the openapi spec from the app to openapi.json.
 * due to the way nest works, we have to turn the app on to generate the document (we cant do it at build time).
 * Therefore, this method turns the app on (expecting dependencies to be running), generates the sepc, and then turns off.
 */
export async function generateOpenApiSpec() {
  const app = await NestFactory.create(AppModule);

  const document = buildOpenApiDoc(app);
  writeFileSync('./openapi.json', JSON.stringify(document, null, 2), {
    encoding: 'utf8',
  });

  await app.close();
}
