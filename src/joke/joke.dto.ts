import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateJokeDto {
  @ApiProperty()
  public readonly category: string;

  @ApiProperty()
  readonly type: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly content: string;
}

export class JokeDto {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  readonly category: string;

  @ApiProperty()
  readonly type: string;

  @ApiProperty()
  readonly content: string;
}
