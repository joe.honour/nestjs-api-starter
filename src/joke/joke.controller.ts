import {
  Body,
  Controller,
  Get,
  Logger,
  NotFoundException,
  Param,
  Post,
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { CreateJokeDto, JokeDto } from './joke.dto';
import { JokeService } from './joke.service';

@Controller('jokes')
export class JokeController {
  private readonly logger = new Logger(JokeController.name);

  constructor(private readonly jokeService: JokeService) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
    type: JokeDto,
  })
  @ApiResponse({ status: 400, description: 'The request was not valid' })
  async createJoke(@Body() createJokeDto: CreateJokeDto): Promise<JokeDto> {
    const addedJoke = await this.jokeService.addJoke(createJokeDto);
    return addedJoke;
  }

  @Get('joke-of-the-day')
  @ApiResponse({
    status: 200,
    description: 'The joke was successfully found.',
    type: JokeDto,
  })
  @ApiResponse({
    status: 500,
    description: 'There was a problem querying the external joke api.',
  })
  async getJokeOfTheDay(): Promise<JokeDto> {
    this.logger.log('test request hit');
    const jokeOfTheDay = await this.jokeService.getJokeOfTheDay();
    return jokeOfTheDay;
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'The joke was successfully found.',
    type: JokeDto,
  })
  @ApiResponse({
    status: 400,
    description: 'The joke with the given id was not found.',
  })
  async getJoke(@Param('id') id: string): Promise<JokeDto> {
    const possibleJoke = await this.jokeService.getJoke(id);

    if (possibleJoke == null) {
      throw new NotFoundException();
    }

    return possibleJoke;
  }
}
