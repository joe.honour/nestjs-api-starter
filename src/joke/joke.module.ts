import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import { JokeController } from './joke.controller';
import { Joke, JokeSchema } from './joke.schema';
import { JokeService } from './joke.service';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([{ name: Joke.name, schema: JokeSchema }]),
  ],
  controllers: [JokeController],
  providers: [JokeService],
})
export class JokeModule {}
