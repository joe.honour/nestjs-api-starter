import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type JokeDocument = Joke & Document;

@Schema()
export class Joke {
  @Prop()
  category: string;

  @Prop()
  type: string;

  @Prop()
  content: string;
}

export const JokeSchema = SchemaFactory.createForClass(Joke);
