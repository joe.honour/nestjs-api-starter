import {
  HttpService,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { isValidObjectId, Model } from 'mongoose';
import { ConfigService } from '../config/config.service';
import { CreateJokeDto, JokeDto } from './joke.dto';
import { Joke, JokeDocument } from './joke.schema';

@Injectable()
export class JokeService {
  private readonly logger = new Logger(JokeService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    @InjectModel(Joke.name) private readonly jokeModel: Model<JokeDocument>,
  ) {}

  /**
   * creates a joke in the database.
   * the joke will be assigned the id from the mongo _id field.
   * @param joke the joke to add
   */
  async addJoke(joke: CreateJokeDto): Promise<JokeDto> {
    const mongoJokeModel = new this.jokeModel(joke);
    const createdJoke = await mongoJokeModel.save();

    return {
      id: createdJoke.id,
      category: createdJoke.category,
      type: createdJoke.type,
      content: createdJoke.content,
    };
  }

  /**
   * gets a joke based on the id.
   * as the id of a joke is the mongo _id field, we must make sure the id is a valid mongo id before
   * executing the query (otherwise mongo throws an exception). if the id is not valid, the joke
   * can't exist and we return null.
   * @param id the id of the joke to find
   */
  async getJoke(id: string): Promise<JokeDto | undefined> {
    if (!isValidObjectId(id)) {
      return null;
    }

    const nullableJoke = await this.jokeModel.findById(id).exec();

    if (nullableJoke) {
      return {
        id: nullableJoke.id,
        category: nullableJoke.category,
        type: nullableJoke.type,
        content: nullableJoke.content,
      };
    }

    return null;
  }

  /**
   * gets the joke of the day from the external joke api. {@see https://jokes.one/api/joke/#jod-json}
   * if any exception occurs, an internal server error is thrown.
   */
  async getJokeOfTheDay(): Promise<JokeDto | undefined> {
    try {
      const response = await this.httpService
        .get(`${this.configService.externalJokeApiConnectionString}/jod`, {
          headers: { 'Content-Type': 'application/json' },
        })
        .toPromise();

      return {
        id: response.data.contents.jokes[0].joke.id,
        category: response.data.contents.jokes[0].category,
        type: 'external-api',
        content: response.data.contents.jokes[0].joke.text,
      };
    } catch {
      throw new InternalServerErrorException(
        'failed to query external joke api',
      );
    }
  }
}
