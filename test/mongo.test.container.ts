import { GenericContainer, StartedTestContainer } from 'testcontainers';

export class MongoTestContainer {
  private container: GenericContainer;

  constructor() {
    this.container = new GenericContainer('mongo', '4.4.2')
      .withExposedPorts(27017)
      .withEnv('MONGO_INITDB_ROOT_USERNAME', 'test')
      .withEnv('MONGO_INITDB_ROOT_PASSWORD', 'test');
  }

  public async start(): Promise<MongoRunningContainer> {
    const runningContainer = await this.container.start();

    const stream = await runningContainer.logs();
    stream
      .on('err', (line) => console.error(line))
      .on('end', () => console.log('container stopped'));

    return new MongoRunningContainer(runningContainer);
  }
}

export class MongoRunningContainer {
  private container: StartedTestContainer;

  constructor(container: StartedTestContainer) {
    this.container = container;
  }

  public getConnectionString(): string {
    return `mongodb://test:test@${this.container.getHost()}:${this.container.getMappedPort(
      27017,
    )}`;
  }

  public async stop() {
    await this.container.stop();
  }
}
