import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { assert } from 'console';

import {
  MongoRunningContainer,
  MongoTestContainer,
} from './mongo.test.container';
import {
  WireMockRunningContainer,
  WireMockTestContainer,
} from './wiremock.test.container';

describe('JokeController (e2e)', () => {
  let app: INestApplication;
  let mongoContainer: MongoRunningContainer;
  let mockJokeApi: WireMockRunningContainer;

  beforeAll(async () => {
    mockJokeApi = await new WireMockTestContainer().start();
    mongoContainer = await new MongoTestContainer().start();
    process.env.MONGO_DB_CONNECTION_URL = mongoContainer.getConnectionString();
    process.env.JOKE_API_CONNECTION_STRING = mockJokeApi.getConnectionString();
  });

  afterAll(async () => {
    await mockJokeApi.stop();
    await mongoContainer.stop();
  });

  afterEach(async () => {
    await app.close();
  });

  beforeEach(async () => {
    mockJokeApi.clearMappings();

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET joke of the day successfully from external api returns 200', async (done) => {
    await mockJokeApi.stubFor({
      request: {
        url: '/jod',
        method: 'GET',
      },
      response: {
        status: 200,
        jsonBody: {
          success: {
            total: 1,
          },
          contents: {
            jokes: [
              {
                description: 'Joke of the day ',
                language: 'en',
                background: '',
                category: 'jod',
                date: '2021-01-07',
                joke: {
                  title: 'Lazy Weather',
                  lang: 'en',
                  length: '72',
                  clean: '1',
                  racial: '0',
                  id: 'succcess-id',
                  text:
                    'the only joke is the test that pretends to make http calls',
                },
              },
            ],
            copyright: '2019-20 https://jokes.one',
          },
        },
      },
    });

    request(app.getHttpServer())
      .get('/jokes/joke-of-the-day')
      .expect(200, {
        id: 'succcess-id',
        category: 'jod',
        type: 'external-api',
        content: 'the only joke is the test that pretends to make http calls',
      })
      .then(() => done());
  });

  it('GET joke of the day from external api fails returns 500', async (done) => {
    await mockJokeApi.stubFor({
      request: {
        url: '/jod',
        method: 'GET',
      },
      response: {
        status: 500,
      },
    });

    request(app.getHttpServer())
      .get('/jokes/joke-of-the-day')
      .expect(500, {
        statusCode: 500,
        message: 'failed to query external joke api',
        error: 'Internal Server Error',
      })
      .then(() => done());
  });

  it('GET joke that does not exist should return 404', async (done) => {
    request(app.getHttpServer())
      .get('/jokes/test')
      .expect(404, {
        statusCode: 404,
        message: 'Not Found',
      })
      .then(() => done());
  });

  it('GET joke that exists, created with POST request, should return 200', async (done) => {
    request(app.getHttpServer())
      .post('/jokes')
      .send({ category: 'test', type: 'programming', content: 'hahahahah' })
      .set('Content-Type', 'application/json')
      .expect(201)
      .end((err, res) => {
        const jokeId = res.body.id;
        request(app.getHttpServer())
          .get(`/jokes/${jokeId}`)
          .expect(200)
          .end((err, res) => {
            assert(res.body.category, 'test');
            assert(res.body.type, 'programming');
            assert(res.body.content, 'hahahahah');
            done();
          });
      });
  });

  it('POST joke in the correct format should return 201', async (done) => {
    request(app.getHttpServer())
      .post('/jokes')
      .send({ category: 'test', type: 'programming', content: 'hahahahah' })
      .set('Content-Type', 'application/json')
      .expect(201)
      .then((res) => {
        assert(res.body.category, 'test');
        assert(res.body.type, 'programming');
        assert(res.body.content, 'hahahahah');
        done();
      });
  });

  it('POST joke with no content should return 400', async () => {
    request(app.getHttpServer())
      .post('/jokes')
      .send({ category: 'test', type: 'programming', content: '' })
      .set('Content-Type', 'application/json')
      .expect(400);
  });
});
