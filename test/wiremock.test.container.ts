import { GenericContainer, StartedTestContainer } from 'testcontainers';
import axios from 'axios';

export class WireMockTestContainer {
  private container: GenericContainer;

  constructor() {
    this.container = new GenericContainer(
      'rodolpheche/wiremock',
      'main',
    ).withExposedPorts(8080);
  }

  public async start(): Promise<WireMockRunningContainer> {
    try {
      const runningContainer = await this.container.start();

      const stream = await runningContainer.logs();
      stream
        .on('err', (line) => console.error(line))
        .on('end', () => console.log('container stopped'));

      return new WireMockRunningContainer(runningContainer);
    } catch (error) {
      console.log(error);
    }
  }
}

export class WireMockRunningContainer {
  private container: StartedTestContainer;

  constructor(container: StartedTestContainer) {
    this.container = container;
  }

  public getConnectionString(): string {
    return `http://${this.container.getHost()}:${this.container.getMappedPort(
      8080,
    )}`;
  }

  public async clearMappings() {
    try {
      await axios.delete(`${this.getConnectionString()}/__admin/mappings`);
    } catch (error) {
      console.log('error deleting mappings from wiremock container', error);
    }
  }

  /**
   * creates a mapping in the running wiremock container.
   * see: http://wiremock.org/docs/request-matching/
   * and: http://wiremock.org/docs/running-standalone/
   * @param wireMockBody the body for configuring a new wiremock mapping, see related docs.
   */
  public async stubFor(wireMockBody: any) {
    try {
      await axios.post(
        `${this.getConnectionString()}/__admin/mappings/new`,
        wireMockBody,
      );
    } catch (error) {
      console.log('error creating mappings in wiremock container', error);
    }
  }

  public async stop() {
    await this.container.stop();
  }
}
